import 'package:booklibrary/utils/shared_pref/shared_pref_helper.dart';
import 'package:flutter/widgets.dart';

class AppTranslations {
  Locale locale;
  static Map<dynamic, dynamic> _localisedValues;
  AppTranslations(Locale locale) {
    this.locale = locale;
    _localisedValues = null;
  }

  static Map<String, Map<String, String>> translatedValues = {
    'ru': {
      'price': 'Цена',
      'description': 'Описание',
      'showMore': 'Показать больше',
      'showLess': 'Показать меньше',
      'similarBook': 'Похожие книги',
      'newBook': 'Новая книга',
      'editBook': 'Редактировать книгу',
      'requiredField': 'Обязательное поле',
      'name': 'Название',
      'author': 'Автор',
      'publishedYear': 'Год публикации',
      'genre': 'Жанр',
      'rating': 'Рейтинг',
      'coverPhoto': 'Фото на обложке',
      'paste': 'Вставить',
      'create': 'Создать',
      'update': 'Обновить',
      'edit': 'Редактировать',
      'delete': 'Удалить',
      'deleteDesc': 'Вы действительно хотите это удалить?',
      'yes': 'Да',
      'no': 'Нет',
      'saved': 'Сохранено',
      'ok': 'Хорошо',
      'settings': 'Настройки',
      'language': 'Языки',
      'books': 'Книги',
      'fantasy': 'Фантастика',
      'adventure': 'Приключение',
      'romance': 'Романтика',
      'dystopian': 'Антиутопия',
      'mystery': 'Тайна',
      'horror': 'Ужас',
      'thriller': 'Триллер',
      'historicalFiction': 'Историческая фантастика',
      'scienceFiction': 'Научная фантастика',
      'cooking': 'Готовка',
      'art': 'Искусство',
      'history': 'История',
      'travel': 'Путешествие',
      'humor': 'Юмор',
      'children': 'Для детей',
      'share': 'Поделиться'
    },
    'uz': {
      'price': 'Narxi',
      'description': 'tavsifi',
      'showMore': 'Ko\'proq ko\'rsatish',
      'showLess': 'Kamroq ko\'rsatish',
      'similarBook': 'Shu kabi kitoblar',
      'newBook': 'Yangi kitob',
      'editBook': 'Kitobni tahrirlash',
      'requiredField': 'Majburiy maydon',
      'name': 'Ismi',
      'author': 'Muallif',
      'publishedYear': 'Chiqarilgan yili',
      'genre': 'Janr',
      'rating': 'Baho',
      'coverPhoto': 'Muqova fotosurati',
      'paste': 'Joylashtirish',
      'create': 'Yaratish',
      'update': 'Yangilash',
      'edit': 'Tahrirlash',
      'delete': 'O\'chirish',
      'deleteDesc': 'Haqiqatan ham buni o‘chirmoqchimisiz?',
      'yes': 'Xa',
      'no': 'Yoq',
      'saved': 'Saqlandi',
      'ok': 'Tushunarli',
      'settings': 'Sozlamalar',
      'language': 'Tillar',
      'books': 'Kitoblar',
      'fantasy': 'Fantaziya',
      'adventure': 'Sarguzasht',
      'romance': 'Romantika',
      'dystopian': 'Distopiya',
      'mystery': 'Sir',
      'horror': 'Dahshat',
      'thriller': 'Triller',
      'historicalFiction': 'Tarixiy fantastika',
      'scienceFiction': 'Ilmiy fantastika',
      'cooking': 'Pishirish',
      'art': 'San\'at',
      'history': 'Tarix',
      'travel': 'Sayohat',
      'humor': 'Hazillar',
      'children': 'Bolalar uchun',
      'share': 'Ulashish'
    },
    'en': {
      'price': 'Price',
      'description': 'Description',
      'showMore': 'Show more',
      'showLess': 'Show less',
      'similarBook': 'Similar books',
      'newBook': 'New book',
      'editBook': 'Edit book',
      'requiredField': 'Required field',
      'name': 'Name',
      'author': 'Author',
      'publishedYear': 'Published year',
      'genre': 'Genre',
      'rating': 'Rating',
      'coverPhoto': 'Cover photo',
      'paste': 'Paste',
      'create': 'Create',
      'update': 'Update',
      'edit': 'Edit',
      'delete': 'Delete',
      'deleteDesc': 'Are you sure you want to delete this?',
      'yes': 'Yes',
      'no': 'No',
      'saved': 'Saved',
      'ok': 'Ok',
      'settings': 'Settings',
      'language': 'Languages',
      'books': 'Books',
      'fantasy': 'Fantasy',
      'adventure': 'Adventure',
      'romance': 'Romance',
      'dystopian': 'Dystopian',
      'mystery': 'Mystery',
      'horror': 'Horror',
      'thriller': 'Thriller',
      'historicalFiction': 'Historical fiction',
      'scienceFiction': 'Science Fiction',
      'cooking': 'Cooking',
      'art': 'Art',
      'history': 'History',
      'travel': 'Travel',
      'humor': 'Humor',
      'children': 'Childen\'s',
      'share': 'Share'
    }
  };

  static AppTranslations of(BuildContext context) {
    return Localizations.of<AppTranslations>(context, AppTranslations);
  }

  static Future<AppTranslations> load(Locale locale) async {
    AppTranslations appTranslations = AppTranslations(locale);
    _localisedValues = translatedValues[locale.languageCode];
    return appTranslations;
  }

  static Future<String> fetchLocale() async {
    return await ShPrefHelper.appLang();
  }

  get currentLanguage => locale.languageCode;

  String text(String key) {
    return _localisedValues[key] ?? "$key not found";
  }
}
