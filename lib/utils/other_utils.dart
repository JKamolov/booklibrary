import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';

abstract class Utils {
  static String getGenreText(BuildContext context, int type) {
    switch (type) {
      case 0:
        return AppTranslations.of(context).text('fantasy');
        break;
      case 1:
        return AppTranslations.of(context).text('adventure');
        break;
      case 2:
        return AppTranslations.of(context).text('romance');
        break;
      case 3:
        return AppTranslations.of(context).text('dystopian');
        break;
      case 4:
        return AppTranslations.of(context).text('mystery');
        break;
      case 5:
        return AppTranslations.of(context).text('horror');
        break;
      case 6:
        return AppTranslations.of(context).text('thriller');
        break;
      case 7:
        return AppTranslations.of(context).text('scienceFiction');
        break;
      case 8:
        return AppTranslations.of(context).text('cooking');
        break;
      case 9:
        return AppTranslations.of(context).text('art');
        break;
      case 10:
        return AppTranslations.of(context).text('history');
        break;
      case 11:
        return AppTranslations.of(context).text('travel');
        break;
      case 12:
        return AppTranslations.of(context).text('humor');
        break;
      case 13:
        return AppTranslations.of(context).text('children');
        break;
      default:
        return AppTranslations.of(context).text('fantasy');
    }
  }
}
