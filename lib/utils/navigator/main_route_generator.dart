import 'package:booklibrary/constants/values/route_constants.dart';
import 'package:booklibrary/ui/pages/book_create/book_create.dart';
import 'package:booklibrary/ui/pages/book_page/book_page.dart';
import 'package:booklibrary/ui/pages/home/home_page.dart';
import 'package:booklibrary/ui/pages/settings_page/settings_page.dart';
import 'package:flutter/material.dart';

class MainRouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case MyRoutes.homePage:
        return MaterialPageRoute<bool>(builder: (_) => HomePage());
      case MyRoutes.bookPage:
        return MaterialPageRoute<bool>(
            builder: (_) => BookPage(
                  book: settings.arguments,
                ));
      case MyRoutes.bookCreate:
        return MaterialPageRoute<bool>(
            builder: (_) => BookCreate(
                  book: settings.arguments,
                ));
      case MyRoutes.settingsPage:
        return MaterialPageRoute<bool>(builder: (_) => SettingaPage());
      default:
        return null;
      // return _errorRoute();
    }
  }
  //  static Route<dynamic> _errorRoute() {
  //   return MaterialPageRoute(builder: (_) => ErrorPage());
  // }
}
