import 'package:booklibrary/constants/values/shared_pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShPrefHelper {
  static SharedPreferences prefs;
  static Future<void> init() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
  }

  static dispose() => prefs = null;
  static Future<bool> clear() => prefs.clear();

  static appLang() => prefs.getString(ShPrefKeys.language);
  static setAppLang(String value) =>
      prefs.setString(ShPrefKeys.language, value);

  static firstLog() => prefs.getBool(ShPrefKeys.firsLog) ?? true;
  static setFirtLog(bool value) => prefs.setBool(ShPrefKeys.firsLog, value);
}
