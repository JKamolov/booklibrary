import 'package:booklibrary/models/api/base/base.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:chopper/chopper.dart';

class CustomDataConverter extends JsonConverter {
  @override
  Response<BodyType> convertResponse<BodyType, SingleItemType>(
      Response response) {
    final Response dynamicResponse = super.convertResponse(response);
    var body = dynamicResponse.body;
    if (body is String) return response;
    if (body is bool) return response;
    final BodyType customBody =
        convertToCustomObject<BodyType, SingleItemType>(body);

    return dynamicResponse.replace<BodyType>(
      body: customBody,
    );
  }

  BodyType convertToCustomObject<BodyType, SingleItemType>(dynamic element) {
    if (element is List)
      return deserializeListOf<BodyType, SingleItemType>(element);
    else
      return deserialize<SingleItemType>(element);
  }

  dynamic deserializeListOf<BodyType, SingleItemType>(
    List dynamicList,
  ) {
    List<SingleItemType> list = dynamicList
        .map<SingleItemType>((element) => deserialize<SingleItemType>(element))
        .toList();
    return list;
  }

  dynamic deserialize<SingleItemType>(Map json) {
    switch (SingleItemType) {
      case BaseResponse:
        return BaseResponse.fromJson(json);
        break;
      case Book:
        return Book.fromJson(json);
        break;
      default:
        return null;
    }
  }
}
