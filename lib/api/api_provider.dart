import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/io_client.dart' as http;
import 'package:booklibrary/services/api/book_service.dart';
import 'custom_converter.dart';

class ApiProvider {
  static ChopperClient _client;
  static BookService bookApi;

  ///Services
  static create({BuildContext context}) {
    _client = ChopperClient(
        client: http.IOClient(
            HttpClient()..connectionTimeout = Duration(seconds: 20)),
        services: [
          BookService.create(),
        ],
        interceptors: getInterceptors(context),
        converter: CustomDataConverter());
    bookApi = _client.getService<BookService>();
  }

  static List getInterceptors(BuildContext context) {
    List interceptors = List();
    interceptors.add(HttpLoggingInterceptor());

    return interceptors;
  }

  static dispose() {
    _client.dispose();
  }
}
