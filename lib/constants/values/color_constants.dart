import 'package:flutter/material.dart';

abstract class MyColors {
  static const accent = Color(0xFFE0BD58);
  static const green = Color(0xFF21A73F);
  static const red = Color(0xFFFD4141);
  static const violet = Color(0xFF626BD9);
  static const yellow = Color(0xFFF8B03E);
  static const blue = Color(0xFF198CFE);
  static const orange = Color(0xFFF39314);
  static const myMessage = Color(0xFFEF6108);

  static const orange25 = Color(0x0DF39314);

  static const backgroud = Color(0xFFF7F7FD);
  static const backgroundColor = Color(0xFFf4f4f4);
  static const divider = Color(0xFFF0F3F6);
  static const border = Color(0xFFD9E2E9);
  static const additionalText = Color(0xFF8790A6);
  static const icon = Color(0xFF586470);
  static const heading = Color(0xFF1D1D1D);

  static const heading04 = Color(0x0D1D1D1D);
  static const heading30 = Color(0x4D1D1D1D);
  static const greyLightest = Color(0xFFF7F8FD); //For InkWell
  static const border25 = Color(0x0D9E2E9); //For InkWell

  static const greyDisable = Color(0xFFBEC1C4);
  static const secondaryText = Color(0xFF838A9A);
  static const shadowColor = Color(0xFFF1F1F1);
}
