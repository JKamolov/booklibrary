abstract class MyRoutes {
  static const homePage = '/home';
  static const bookPage = '/bookPage';
  static const bookCreate = '/bookCreate';
  static const settingsPage = '/settingsPage';
}
