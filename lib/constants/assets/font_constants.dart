abstract class MyFonts {
  static const regular = 'Montserrat';
  static const black = 'Montserrat-Black';
  static const semibold = 'Montserrat-SemiBold';
  static const medium = 'Montserrat-Medium';
  static const bold = 'Montserrat-Bold';
}
