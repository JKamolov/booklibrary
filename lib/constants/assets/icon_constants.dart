abstract class MyIcons {
  static const base = 'assets/icons';
  static const share = '$base/share.svg';
  static const paste = '$base/paste.svg';
}
