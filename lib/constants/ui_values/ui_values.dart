import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';

List<Book> recommendedBooks = [
  Book(
      title: 'The Innocents',
      author: 'Michael Crumney',
      description:
          'A brother and sister are orphaned in an isolated cove on Newfoundland\'s northern coastline. Their home is a stretch of rocky shore governed by the feral ocean, by a relentless pendulum of abundance and murderous scarcity. Still children with only the barest notion of the outside world, they have nothing but the family\'s boat and the little knowledge passed on haphazardly by their mother and father to keep them.',
      price: '99\$',
      genre: 0,
      rating: 4,
      imageUrl:
          'https://images-na.ssl-images-amazon.com/images/I/81vgGks%2BIKL.jpg',
      publishedYear: 2019),
  Book(
      title: 'Empire of Wild',
      author: 'Cherie Dimaline',
      description:
          'From the author of the YA-crossover hit The Marrow Thieves, a propulsive, stunning and sensuous novel inspired by the traditional Métis story of the Rogarou - a werewolf-like creature that haunts the roads and woods of Métis communities. A messed-up, grown-up, Little Red Riding Hood.',
      price: '99\$',
      genre: 5,
      rating: 4,
      imageUrl:
          'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1555026643l/45024447.jpg',
      publishedYear: 2019),
  Book(
      title: 'From the Ashes',
      author: 'Jesse Thistle',
      description:
          'In this extraordinary and inspiring debut memoir, Jesse Thistleonce a high school dropout and now a rising Indigenous scholarchronicles his life on the streets and how he overcame trauma and addiction to discover the truth about who he is.',
      price: '99\$',
      genre: 2,
      rating: 4,
      imageUrl:
          'https://images-na.ssl-images-amazon.com/images/I/41ucsl9V3YL._SX331_BO1,204,203,200_.jpg',
      publishedYear: 2019),
  Book(
      title: 'Dual Citizens',
      author: 'Alix Ohlin',
      description:
          'Lark and Robin are half-sisters whose similarities end at being named for birds. While Lark is shy and studious, Robin is wild and artistic. Raised in Montreal by their disinterested single mother, they form a fierce team in childhood regardless of their differences. As they grow up, Lark excels at school and Robin becomes an extraordinary pianist. At seventeen, Lark flees to America to attend college, where she finds her calling in documentary films, and her sister soon joins her. Later, in New York City, they find themselves tested: Lark struggles with self-doubt, and Robin chafes against the demands of Juilliard. Under pressure, their bond grows strained and ultimately is broken, and their paths abruptly diverge. Years later, Lark\'s life is in tatters and Robin\'s is wilder than ever. As Lark tries to take charge of her destiny, she discovers that despite the difficulties of their relationship, there is only one person she can truly rely on: her sister.',
      price: '99\$',
      genre: 0,
      rating: 4,
      imageUrl:
          'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/3491/9780349134680.jpg',
      publishedYear: 2019),
  Book(
      title: 'Lampedusa',
      author: 'Steven Price',
      description:
          'In sun-drenched Sicily, among the decadent Italian aristocracy of the late 1950s, Giuseppe Tomasi, the last prince of Lampedusa, struggles to complete the novel that will be his lasting legacy, The Leopard. With a firm devotion to the historical record, Lampedusa leaps effortlessly into the mind of the writer and inhabits the complicated heart of a man facing down the end of his life, struggling to make something of lasting worth, while there is still time. Achingly beautiful and elegantly conceived, Lampedusa is an intensely moving story of one man\'s awakening to the possibilities of life, intimately woven against the transformative power of a great work of art',
      price: '99\$',
      genre: 0,
      rating: 4.5,
      imageUrl:
          'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1551785120l/43416753.jpg',
      publishedYear: 2019),
  Book(
      title: 'Immigrant City',
      author: 'David Bezmozgis',
      description:
          'In the title story, a father and his young daughter stumble into a bizarre version of his immigrant childhood. A mysterious tech conference brings a writer to Montreal, where he discovers new designs on the past in “How It Used to Be.” A grandfather’s Yiddish letters expose a love affair and a wartime secret in “Little Rooster.” In “Childhood,” Mark’s concern about his son’s phobias evokes a shameful incident from his own adolescence. In “Roman’s Song,” Roman’s desire to help a new immigrant brings him into contact with a sordid underworld. At his father’s request, Victor returns to Riga, the city of his birth, where his loyalties are tested by the man he might have been in “A New Gravestone for an Old Grave.” And, in the noir-inspired “The Russian Riviera,” Kostya leaves Russia to pursue a boxing career only to find himself working as a doorman in a garish nightclub in the Toronto suburbs.',
      price: '69\$',
      genre: 5,
      rating: 4,
      imageUrl:
          'https://images-na.ssl-images-amazon.com/images/I/81hnHu96sHL.jpg',
      publishedYear: 2019),
];

List<String> gendres(BuildContext context) {
  return [
    AppTranslations.of(context).text('fantasy'),
    AppTranslations.of(context).text('adventure'),
    AppTranslations.of(context).text('romance'),
    AppTranslations.of(context).text('dystopian'),
    AppTranslations.of(context).text('mystery'),
    AppTranslations.of(context).text('horror'),
    AppTranslations.of(context).text('thriller'),
    AppTranslations.of(context).text('scienceFiction'),
    AppTranslations.of(context).text('cooking'),
    AppTranslations.of(context).text('art'),
    AppTranslations.of(context).text('history'),
    AppTranslations.of(context).text('travel'),
    AppTranslations.of(context).text('humor'),
    AppTranslations.of(context).text('children'),
    AppTranslations.of(context).text('fantasy'),
  ];
}
