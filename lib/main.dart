import 'package:booklibrary/utils/localization/application.dart';
import 'package:booklibrary/utils/navigator/locator.dart';
import 'package:booklibrary/utils/navigator/main_route_generator.dart';
import 'package:booklibrary/utils/navigator/navigation_service.dart';
import 'package:booklibrary/utils/shared_pref/shared_pref_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logging/logging.dart';

import 'constants/values/color_constants.dart';
import 'constants/values/route_constants.dart';
import 'services/sql/sql_service.dart';
import 'ui/widgets/my_scroll_behavior.dart';
import 'utils/localization/app_translations_delegate.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ShPrefHelper.init();
  await SqlServce.initDB();
  await SqlServce.initTables();

  setupLocator();
  setUpLogging();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: Colors.black,
    systemNavigationBarDividerColor: Colors.black,
    systemNavigationBarIconBrightness: Brightness.light,
  ));

  return runApp(MyApp());
}

setUpLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((data) {
    debugPrint('${data.level.name} ${data.time} ${data.message} ');
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppTranslationsDelegate _newLocaleDelegate;
  NavigationService navigationService;

  @override
  void initState() {
    navigationService = locator<NavigationService>();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
    setLanguage();
    super.initState();
  }

  @override
  void dispose() {
    ShPrefHelper.dispose();
    SqlServce.closeDb();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.portraitDown,
    //   DeviceOrientation.portraitUp,
    //   DeviceOrientation.landscapeLeft,
    // ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,

      // statusBarBrightness: Brightness.light, //For iOS
      // statusBarIconBrightness: Brightness.dark, //For Android
    ));

    return MaterialApp(
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: MyScrollBehavior(),
          child: child,
        );
      },
      title: 'My Library',
      supportedLocales: application.supportedLocales(),
      theme: ThemeData(
          appBarTheme: AppBarTheme(color: Colors.orange),
          accentColor: MyColors.accent,
          primaryColor: Colors.white,
          primaryColorDark: Colors.white,
          scaffoldBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          highlightColor: MyColors.orange25,
          splashColor: MyColors.orange25,
          hoverColor: MyColors.orange25,
          canvasColor: Colors.transparent),
      localizationsDelegates: [
        _newLocaleDelegate,

        const FallbackCupertinoLocalisationsDelegate(),

        const AppTranslationsDelegate(),
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      initialRoute: MyRoutes.homePage,
      onGenerateRoute: MainRouteGenerator.generateRoute,
      navigatorKey: locator<NavigationService>().navigatorKey,
    );
  }

  setLanguage() async {
    String lang = await ShPrefHelper.appLang();
    if (lang == null) {
      application.onLocaleChanged(Locale('ru'));
    } else {
      application.onLocaleChanged(Locale('$lang'));
    }
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
