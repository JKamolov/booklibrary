import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';

@JsonSerializable()
class Book {
  int id;
  String title;
  String description;
  String author;
  String price;
  @JsonKey(name: 'publishedYear')
  int publishedYear;
  @JsonKey(name: 'imageUrl')
  String imageUrl;
  int genre;
  double rating;

  Book({
    this.author,
    this.description,
    this.genre,
    this.id,
    this.imageUrl,
    this.price,
    this.publishedYear,
    this.rating,
    this.title,
  });

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);

  Map<String, dynamic> toJson() => _$BookToJson(this);
}
