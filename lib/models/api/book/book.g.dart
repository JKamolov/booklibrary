// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) {
  return Book(
    author: json['author'] as String,
    description: json['description'] as String,
    genre: json['genre'] as int,
    id: json['id'] as int,
    imageUrl: json['imageUrl'] as String,
    price: json['price'] as String,
    publishedYear: json['publishedYear'] as int,
    rating: (json['rating'] as num)?.toDouble(),
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'author': instance.author,
      'price': instance.price,
      'publishedYear': instance.publishedYear,
      'imageUrl': instance.imageUrl,
      'genre': instance.genre,
      'rating': instance.rating,
    };
