import 'package:booklibrary/models/api/base/base.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:chopper/chopper.dart';

part 'book_service.chopper.dart';

@ChopperApi(baseUrl: 'local')
abstract class BookService extends ChopperService {
  static BookService create([ChopperClient client]) => _$BookService(client);

  @Get(path: 'getAll')
  Future<Response<List<Book>>> getBooks();

  @Post(path: 'create')
  Future<Response<BaseResponse>> createBook(
    @Body() Map body,
  );

  @Post(path: 'update/{id}')
  Future<Response<BaseResponse>> updateBook(
    @Body() Map body,
    @Path('id') int id,
  );

  @Post(path: 'delete/{id}')
  Future<Response<BaseResponse>> deleteBook(
    @Path('id') int id,
  );
}
