// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$BookService extends BookService {
  _$BookService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = BookService;

  @override
  Future<Response<List<Book>>> getBooks() {
    final $url = 'local/getAll';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<Book>, Book>($request);
  }

  @override
  Future<Response<BaseResponse>> createBook(Map<dynamic, dynamic> body) {
    final $url = 'local/create';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<BaseResponse, BaseResponse>($request);
  }

  @override
  Future<Response<BaseResponse>> updateBook(
      Map<dynamic, dynamic> body, int id) {
    final $url = 'local/update/$id';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<BaseResponse, BaseResponse>($request);
  }

  @override
  Future<Response<BaseResponse>> deleteBook(int id) {
    final $url = 'local/delete/$id';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<BaseResponse, BaseResponse>($request);
  }
}
