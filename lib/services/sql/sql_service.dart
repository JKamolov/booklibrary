import 'package:booklibrary/constants/values/sql_constants.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqlServce {
  static Database db;
  static Future<void> initDB() async {
    if (db == null) {
      db = await openDatabase(
        join(await getDatabasesPath(), SqlConstants.dbName),
      );
    }
  }

  static Future<void> initTables() async {
    Batch batch = db.batch();

    batch.execute(
        'CREATE TABLE IF NOT EXISTS ${SqlConstants.books}(id INTEGER PRIMARY KEY AUTOINCREMENT,  title VARCHAR, author VARCHAR NULLABLE, description TEXT, price VARCHAR, publishedYear INTEGER, imageUrl VARCHAR NULLABLE, genre INTEGER NULLABLE )');

    await batch.commit(noResult: true);
  }

  static closeDb() {
    db.close();
  }
}
