import 'package:booklibrary/constants/values/sql_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/services/sql/sql_service.dart';
import 'package:sqflite/sqflite.dart';

abstract class SqlOp {
  static Future<List<Book>> getBooks() async {
    var bookList = await SqlServce.db
        .rawQuery('SELECT * FROM ${SqlConstants.books} ORDER BY ID DESC');
    Map<String, dynamic> json = {"list": bookList};
    return List<Book>.from(json["list"].map((i) => Book.fromJson(i)));
  }

  static createBook(Book book) async {
    await SqlServce.db.execute(
        'INSERT INTO ${SqlConstants.books} (title, author, description, price, publishedYear, imageUrl,genre ) VALUES (?, ?, ?, ?, ?,?,?)',
        [
          book.title,
          book.author,
          book.description,
          book.price,
          book.publishedYear,
          book.imageUrl,
          book.genre
        ]);
  }

  static Future updateBook(Book book) async {
    await SqlServce.db.execute(
        'UPDATE ${SqlConstants.books} SET title=?, author=?, description=?, price=?, publishedYear=?, imageUrl=?,genre=?  WHERE id=?',
        [
          book.title,
          book.author,
          book.description,
          book.price,
          book.publishedYear,
          book.imageUrl,
          book.genre,
          book.id
        ]);
  }

  static Future deleteBook(int bookId) async {
    await SqlServce.db
        .execute('DELETE FROM ${SqlConstants.books} WHERE id=?', [bookId]);
  }

  static Future clearAllTables() async {
    Batch batch = SqlServce.db.batch();
    batch.execute('DELETE FROM ${SqlConstants.books}');

    batch.commit();
  }

  static Future fillWithData(List<Book> books) async {
    Batch batch = SqlServce.db.batch();

    books.forEach((book) {
      // Map<String, dynamic> poem = element.toJson();
      batch.execute(
          'INSERT INTO ${SqlConstants.books} (title, author, description, price, publishedYear, imageUrl,genre ) VALUES (?, ?, ?, ?, ?,?,?)',
          [
            book.title,
            book.author,
            book.description,
            book.price,
            book.publishedYear,
            book.imageUrl,
            book.genre
          ]);
    });
    await batch.commit(noResult: true);
  }
}
