import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DefaultTextField extends StatelessWidget {
  final bool isRequired;
  TextEditingController controller;
  final String labelText;
  final String hintText;
  final bool enabled;
  final FocusNode focusNode;
  final TextInputType inputType;
  final bool showRequiredSign;
  final bool password;
  final Function onFieldSubmitted;
  final TextInputAction textInputAction;
  final Function onChanged;
  final Widget leadingIcon;
  final Widget trailingWidget;
  final String leadingText;
  final List<TextInputFormatter> inputFormatters;
  final FormFieldValidator<String> customValidator;
  final Function onTap;
  final Function onSaved;
  final Color cursorColor;
  final int maxLines;
  final String initialValue;
  final EdgeInsets contentPadding;
  final bool autoValidate;

  DefaultTextField({
    Key key,
    this.isRequired = false,
    this.controller,
    this.labelText,
    this.leadingIcon,
    this.password = false,
    this.hintText,
    this.enabled = true,
    this.contentPadding,
    this.inputFormatters,
    this.leadingText,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTap,
    this.trailingWidget,
    this.onSaved,
    this.showRequiredSign = false,
    this.textInputAction = TextInputAction.done,
    this.inputType = TextInputType.text,
    this.customValidator,
    this.cursorColor = MyColors.accent,
    this.maxLines,
    this.initialValue,
    this.autoValidate = false,
    this.focusNode,
  }) : super(key: key);
  setText() {
    if (controller == null && initialValue != null) {
      controller = TextEditingController();
      controller.text = initialValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    setText();

    return TextFormField(
      focusNode: focusNode,
      validator: (text) {
        if (isRequired && text.isEmpty) {
          return AppTranslations.of(context).text('requiredField');
        }
        if (customValidator != null) {
          return customValidator(text);
        }
      },
      onFieldSubmitted: (String value) {
        if (onFieldSubmitted != null) {
          onFieldSubmitted(value);
        }
      },
      onChanged: (String value) {
        if (onChanged != null) {
          onChanged(value);
        }
      },
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      onSaved: (String value) {
        if (onSaved != null) {
          onSaved(value);
        }
      },
      autovalidate: autoValidate,
      textInputAction: textInputAction,
      controller: controller,
      keyboardType: inputType,
      inputFormatters: inputFormatters == null ? [] : inputFormatters,
      obscureText: password,
      cursorColor: cursorColor,
      enabled: enabled,
      maxLines: maxLines == null ? 1 : maxLines,
      style: TextStyle(
        fontSize: 17,
        fontFamily: MyFonts.regular,
        color: enabled ? Colors.black : MyColors.additionalText,
      ),
      decoration: InputDecoration(
        prefixIcon: leadingIcon,
        contentPadding:
            contentPadding == null ? EdgeInsets.all(0) : contentPadding,
        suffix: trailingWidget,
        prefixText: leadingText,
        hintText: hintText,
        hintStyle: TextStyle(
          color: MyColors.additionalText,
          fontFamily: MyFonts.regular,
        ),
        labelText: labelText == null
            ? null
            : '$labelText ${showRequiredSign ? '*' : ''}',
        labelStyle: TextStyle(
          color: MyColors.additionalText,
          fontFamily: MyFonts.regular,
        ),
        border: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
      ),
    );
  }
}
