import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'default_textfield.dart';

class DefaultTextFieldContainer extends StatelessWidget {
  final bool isRequired;
  final TextEditingController controller;
  final String labelText;
  final String hintText;
  final bool enabled;
  final FocusNode focusNode;
  final TextInputType inputType;
  final bool showRequiredSign;
  final bool password;
  final Function onFieldSubmitted;
  final TextInputAction textInputAction;
  final double height;
  final Function onChanged;
  final Widget leadingIcon;
  final Widget trailingWidget;
  final String leadingText;
  final List<TextInputFormatter> inputFormatters;
  final FormFieldValidator<String> customValidator;
  final Function onTap;
  final Function onSaved;
  final Color cursorColor;
  final int maxLines;
  final Color borderColor;
  final String initialValue;
  final EdgeInsets contentPadding;
  final bool autoValidate;
  DefaultTextFieldContainer(
      {Key key,
      this.isRequired = false,
      this.controller,
      this.labelText,
      this.leadingIcon,
      this.password = false,
      this.hintText,
      this.enabled = true,
      this.inputFormatters,
      this.leadingText,
      this.onChanged,
      this.height = 67,
      this.onFieldSubmitted,
      this.trailingWidget,
      this.cursorColor,
      this.maxLines,
      this.showRequiredSign = false,
      this.textInputAction = TextInputAction.done,
      this.inputType = TextInputType.text,
      this.customValidator,
      this.focusNode,
      this.onTap,
      this.initialValue,
      this.contentPadding,
      this.borderColor = MyColors.divider,
      this.autoValidate,
      this.onSaved})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        decoration: BoxDecoration(
            color: MyColors.backgroud,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(width: 1, color: borderColor)),
        child: DefaultTextField(
            contentPadding: contentPadding,
            isRequired: isRequired,
            initialValue: initialValue,
            controller: controller,
            labelText: labelText,
            leadingIcon: leadingIcon,
            password: password,
            hintText: hintText,
            cursorColor: cursorColor,
            enabled: enabled,
            inputFormatters: inputFormatters,
            leadingText: leadingText,
            onChanged: onChanged,
            onFieldSubmitted: onFieldSubmitted,
            onTap: onTap,
            trailingWidget: trailingWidget,
            onSaved: onSaved,
            showRequiredSign: showRequiredSign,
            autoValidate: autoValidate,
            textInputAction: textInputAction,
            inputType: inputType,
            customValidator: customValidator,
            maxLines: maxLines,
            focusNode: focusNode));
  }
}
