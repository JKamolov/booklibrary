import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
// import 'package:flutter_advanced_networkimage/provider.dart';
// import 'package:flutter_advanced_networkimage/transition.dart';

class MyNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double width, height;
  final Color placeholderColor;
  final BoxFit fit;
  final double borderRadius;
  final bool hasTopBorderRadius;

  MyNetworkImage({
    this.imageUrl,
    @required this.borderRadius,
    this.height,
    this.width,
    this.hasTopBorderRadius = false,
    this.fit = BoxFit.cover,
    this.placeholderColor = MyColors.greyDisable,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl == null || imageUrl.isEmpty
          ? 'https://www.zumlume.com/assets/frontend/images/default-book.png'
          : imageUrl,
      width: width,
      height: height,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          borderRadius: hasTopBorderRadius
              ? BorderRadius.only(
                  topLeft: Radius.circular(borderRadius),
                  topRight: Radius.circular(borderRadius),
                )
              : BorderRadius.circular(borderRadius),
          image: DecorationImage(
            image: imageProvider,
            fit: fit,
          ),
        ),
      ),
      placeholder: getPlaceholder,
      errorWidget: (context, url, error) {
        // print(error);
        return ClipRRect(
          borderRadius: BorderRadius.circular(borderRadius),
          child: Image.network(
            'https://www.zumlume.com/assets/frontend/images/default-book.png',
            fit: BoxFit.cover,
          ),
        );
      },
    );
  }

  Widget getPlaceholder(context, url) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      direction: ShimmerDirection.ltr,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: placeholderColor,
          borderRadius: BorderRadius.circular(borderRadius),
        ),
      ),
    );
  }
}
