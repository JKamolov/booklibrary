import 'dart:io' as io;
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:flutter/material.dart';

class DefaultBackButton extends StatelessWidget {
  final Color iconColor;
  final Function onTap;
  DefaultBackButton({this.iconColor = MyColors.orange, this.onTap});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Icon(
        io.Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
        color: iconColor,
      ),
      onTap: onTap == null
          ? Navigator.of(context).pop
          : () {
              onTap();
            },
    );
  }
}
