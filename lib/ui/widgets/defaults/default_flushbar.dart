import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

void showMessage(String message, bool isError, BuildContext context,
    {String titleText}) async {
  Flushbar(
    icon: isError
        ? Icon(
            Icons.error,
            color: Colors.white,
          )
        : Icon(Icons.check_circle, color: Colors.white),
    margin: EdgeInsets.fromLTRB(15, 50, 15, 0),
    flushbarPosition: FlushbarPosition.TOP,
    forwardAnimationCurve: Curves.easeInOut,
    borderRadius: 8.0,
    message: message,
    title: titleText ?? null,
    duration: Duration(seconds: 4),
    isDismissible: true,
    backgroundColor: isError ? MyColors.red : MyColors.accent,
  )..show(context);
}
