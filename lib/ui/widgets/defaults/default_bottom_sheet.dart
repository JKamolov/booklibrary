import 'package:flutter/material.dart';

import 'default_bottom_sheet_top_icon.dart';

class DefaultBottomSheet extends StatelessWidget {
  final Widget child;
  final bool addBottomPadding;

  const DefaultBottomSheet(
      {Key key, @required this.child, this.addBottomPadding = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    double bottom = MediaQuery.of(context).padding.bottom;
    double bottom2 = MediaQuery.of(context).viewInsets.bottom;
    return Container(
      padding: EdgeInsets.only(
          top: 8,
          bottom: addBottomPadding
              ? bottom + 16 + bottom2
              : 0), //bottom: bottom + 16 + bottom2),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(24), topRight: Radius.circular(24))),
      child: SingleChildScrollView(
        child: Wrap(
          // mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            BottomSheetTopIcon(),
            SizedBox(
              height: 14,
            ),
            child,
            SizedBox(
              height: bottom + 16 + bottom2,
            )
          ],
        ),
      ),
    );
  }
}
