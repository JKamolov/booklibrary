import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:flutter/material.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';

class BottomSheetTitle extends StatelessWidget {
  final String title;

  BottomSheetTitle({
    this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Align(
        child: Text17(title, fontFamily: MyFonts.bold),
      ),
    );
  }
}
