import 'package:booklibrary/ui/widgets/text/text15_semibold.dart';
import 'package:flutter/material.dart';

import 'default_back_button.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;
  final bool isShadow, automaticallyImplyLeading;
  final Widget leading;
  final Color color;
  MyAppBar(
      {this.title = '',
      this.actions,
      this.isShadow = false,
      this.leading,
      this.color = Colors.white,
      this.automaticallyImplyLeading = true});
  @override
  Widget build(BuildContext context) {
    return Container(
      // decoration: BoxDecoration(boxShadow: isShadow ? tabbarShadow : null),
      child: AppBar(
        leading: leading ?? DefaultBackButton(),
        title: Text15s(title),
        centerTitle: true,
        elevation: 0.0,
        actions: actions,
        backgroundColor: color,
        automaticallyImplyLeading: automaticallyImplyLeading,
      ),
    );
  }

  @override
  Size get preferredSize => AppBar().preferredSize;
}
