import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final double radius;
  final Function onPressed;
  final Color color;
  final double elevation;
  final double hightLightElevation;
  final double height;
  final double padding;
  final Widget child;
  DefaultButton(
      {this.radius = 12,
      @required this.onPressed,
      this.color = Colors.white,
      this.elevation = 0,
      this.height,
      this.padding = 0,
      @required this.child,
      this.hightLightElevation = 0});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation,
      borderRadius: BorderRadius.circular(radius),
      child: MaterialButton(
        elevation: elevation,
        highlightElevation: hightLightElevation,
        padding: EdgeInsets.all(padding),
        height: height,
        color: color,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        onPressed: () {
          onPressed();
        },
        child: child,
      ),
    );
  }
}
