import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:flutter/material.dart';

class Text20b extends StatelessWidget {
  final String data;
  final int maxLines;
  final bool isOverflow;
  final bool isBold;
  final String fontFamily;
  final TextAlign textAlign;
  final Color color;
  final FontWeight fontWeight;
  final double height, letterSpacing;

  Text20b(this.data,
      {this.maxLines,
      this.isOverflow = true,
      this.isBold = false,
      this.fontFamily = MyFonts.bold,
      this.textAlign,
      this.color = Colors.black,
      this.height = 1.5,
      this.fontWeight = FontWeight.normal,
      this.letterSpacing = 1.0});

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      key: key,
      overflow: isOverflow ? TextOverflow.ellipsis : null,
      maxLines: maxLines,
      textAlign: textAlign,
      style: TextStyle(
        fontFamily: fontFamily,
        fontSize: 20,
        color: color,
        height: height,
        fontWeight: fontWeight,
        // letterSpacing: letterSpacing,
      ),
    );
  }
}
