import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  final String data;
  final int maxLines;
  final bool isOverflow;
  final bool isBold;
  final String fontFamily;
  final TextAlign textAlign;
  final Color color;
  final double height, letterSpacing;
  final double fontSize;
  MyText(this.data,
      {this.maxLines,
      this.isOverflow = true,
      this.isBold = false,
      this.fontFamily = MyFonts.regular,
      this.textAlign,
      this.fontSize = 17,
      this.color = Colors.black,
      this.height = 1.5,
      this.letterSpacing = 1.0});

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      key: key,
      overflow: isOverflow ? TextOverflow.ellipsis : null,
      maxLines: maxLines,
      textAlign: textAlign,
      style: TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color: color,
        height: height,
        // letterSpacing: letterSpacing,
      ),
    );
  }
}
