import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:flutter/material.dart';

class Text15 extends StatelessWidget {
  final String data;
  final int maxLines;
  final TextOverflow overflow;
  final bool isBold;
  final String fontFamily;
  final TextAlign textAlign;
  final Color color;
  final double height;

  Text15(
    this.data, {
    this.maxLines,
    this.overflow = TextOverflow.ellipsis,
    this.isBold = false,
    this.fontFamily = MyFonts.regular,
    this.textAlign,
    this.color = Colors.black,
    this.height = 1.5,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      key: key,
      overflow: overflow,
      maxLines: maxLines,
      textAlign: textAlign,
      style: TextStyle(
        fontFamily: fontFamily,
        fontSize: 15,
        color: color,
        height: height,
        // letterSpacing: letterSpacing,
      ),
    );
  }
}
