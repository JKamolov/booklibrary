import 'package:url_launcher/url_launcher.dart';

void launchURL(String urlFrom, bool isWebView) async {
  final url = urlFrom;

  if (await canLaunch(url)) {
    if (isWebView != null && isWebView) {
      await launch(url, forceWebView: true);
    } else {
      await launch(url);
    }
  } else {
    print('Could not launch $url');
  }
}

void launchTel(String phone) async {
  String tel = phone
      .replaceAll('+', '')
      .replaceAll(')', '')
      .replaceAll('(', '')
      .replaceAll(' ', '');
  await launch('tel:+$tel');
}
