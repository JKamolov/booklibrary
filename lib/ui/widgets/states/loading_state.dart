import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MyLoadingState extends StatelessWidget {
  final Color color;
  final double size;
  const MyLoadingState(
      {Key key, this.color = MyColors.orange, this.size = 50.0})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SpinKitFadingCircle(
        color: color,
        size: size,
      ),
    );
  }
}
