import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/ui/widgets/text/text15.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';

class ErrorFiller extends StatelessWidget {
  final String message;
  final Function method;

  ErrorFiller({
    this.message,
    this.method,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(),
          Icon(
            Icons.error_outline,
            color: Colors.black26,
            size: 70.0,
          ),
          SizedBox(
            height: 20.0,
          ),
          Text17(
            AppTranslations.of(context).text('serverUnreachable'),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text15(
            message,
            color: Colors.black26,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 20.0,
          ),
          getButton(context)
        ],
      ),
    );
  }

  Widget getButton(BuildContext context) {
    return Material(
      color: MyColors.orange,
      borderRadius: BorderRadius.circular(20.0),
      child: MaterialButton(
        height: 40.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        onPressed: () {
          method();
        },
        child: Container(
          height: 40,
          child: Center(
            child: Text15(
              AppTranslations.of(context).text('tryagain'),
              color: Colors.white,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
