import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MyOnTopLoadingState extends StatelessWidget {
  final Color color;

  const MyOnTopLoadingState({Key key, this.color = MyColors.accent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Colors.black45,
      color: Colors.white54,

      child: Center(
        child: SpinKitFadingCircle(
          color: color,
          // duration: Duration(minutes: 2),
        ),
      ),
    );
  }
}
