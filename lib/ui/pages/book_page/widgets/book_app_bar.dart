import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/defaults/default_back_button.dart';
import 'package:booklibrary/ui/widgets/text/text15.dart';
import 'package:flutter/material.dart';

class BookAppBar extends StatelessWidget implements PreferredSizeWidget {
  BuildContext context;
  final Book book;
  final bool removeTransparency;
  final AnimationController controller;
  final Animation backgroundTween;
  final Animation iconTween;
  final Animation titleTween;

  BookAppBar({
    this.removeTransparency,
    this.controller,
    this.backgroundTween,
    this.iconTween,
    this.book,
    this.titleTween,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Container(
      width: MediaQuery.of(context).size.width,
      height:
          AppBar().preferredSize.height + MediaQuery.of(context).padding.top,
      child: AnimatedBuilder(
        animation: controller,
        // child: child,
        builder: (BuildContext context, Widget child) {
          return Container(
            child: AppBar(
              leading: DefaultBackButton(
                iconColor: iconTween.value,
              ),
              elevation: 0.0,
              backgroundColor: backgroundTween.value,
              centerTitle: true,
              title: Text15(
                book.title,
                color: titleTween.value,
              ),
              actions: <Widget>[],
            ),
          );
        },
      ),
    );
  }

  @override
  Size get preferredSize => AppBar().preferredSize;
}
