import 'package:booklibrary/blocs/book/book_bloc.dart';
import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/text/default_text.dart';
import 'package:booklibrary/ui/widgets/text/text15.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookDesc extends StatelessWidget {
  final Book book;
  final bool expandableOpen;
  final ExpandableController expandableController;
  BuildContext context;
  BookBloc bloc;

  BookDesc({this.book, this.expandableOpen, this.expandableController});

  @override
  Widget build(BuildContext context) {
    this.context = context;
    bloc = BlocProvider.of<BookBloc>(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 24,
          ),
          Container(
            // padding: EdgeInsets.symmetric(horizontal: 16),
            child: MyText(
              AppTranslations.of(context).text('description'),
              fontFamily: MyFonts.bold,
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          getDescriptionText(),
          book.description.length > 80 ? readMore() : Container(),
          // DefaultDivider()
        ],
      ),
    );
  }

  Widget getDescriptionText() {
    return Container(
      // padding: EdgeInsets.symmetric(horizontal: 16),
      child: ExpandablePanel(
        controller: expandableController,
        collapsed: Align(
          alignment: Alignment.centerLeft,
          child: Text15(
            book.description,
            textAlign: TextAlign.left,
            maxLines: 5,
          ),
        ),
        // tapBodyToCollapse: true,
        expanded: Text17(
          book.description,
          maxLines: 30,
        ),
        // tapHeaderToExpand: false,
        // hasIcon: false,
      ),
    );
  }

  Widget readMore() {
    return DefaultButton(
      radius: 0,
      onPressed: () {
        // expandableController.toggle();
        bloc.add(UpdateExpandableState(!expandableOpen));
      },
      child: Container(
        height: 52,
        width: MediaQuery.of(context).size.width,
        // padding: EdgeInsets.symmetric(horizontal: 16),
        alignment: Alignment.centerLeft,
        child: Text15(
          AppTranslations.of(context)
              .text(expandableOpen ? 'showLess' : 'showMore'),
          color: MyColors.accent,
          textAlign: TextAlign.start,
          fontFamily: MyFonts.medium,
        ),
      ),
    );
  }
}
