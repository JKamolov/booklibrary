import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/defaults/network_image.dart';
import 'package:booklibrary/ui/widgets/text/default_text.dart';
import 'package:booklibrary/ui/widgets/text/text15.dart';
import 'package:booklibrary/ui/widgets/text/text15_semibold.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:booklibrary/utils/other_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class BookInfo extends StatelessWidget {
  final Book book;
  BuildContext context;

  BookInfo({
    this.book,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      height: 130,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          picture(),
          SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                MyText(
                  book.title,
                  fontFamily: MyFonts.semibold,
                  maxLines: 2,
                ),
                Text17(
                  book.author,
                  // color: MyColors.secondaryText,
                ),
                Text15(
                  Utils.getGenreText(context, book.genre),
                  color: MyColors.secondaryText,
                ),
                SizedBox(
                  height: 3,
                ),
                rating(),
                Expanded(
                  child: Container(),
                ),
                bottom()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget rating() {
    return RatingBar(
      initialRating: 3.5, //book.rating.toDouble(),
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      ignoreGestures: true,
      itemSize: 18,
      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );
  }

  Widget bottom() {
    return Row(
      children: <Widget>[
        Text15s(
          '${AppTranslations.of(context).text('price')}: ',
          color: MyColors.accent,
        ),
        Text15s(
          '${book.price} ${book.price.contains('\$') ? '' : '\$'}',
          color: MyColors.accent,
        ),
        SizedBox(
          width: 16,
        ),
      ],
    );
  }

  Widget picture() {
    return Container(
      width: 95,
      height: 130,
      child: MyNetworkImage(
        imageUrl: book.imageUrl,
        borderRadius: 12,
        fit: BoxFit.fill,
      ),
    );
  }
}
