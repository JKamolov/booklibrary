import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/pages/book_page/book_page.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/network_image.dart';
import 'package:flutter/material.dart';

class SimilarBooks extends StatelessWidget {
  final List<Book> allBooks;
  BuildContext context;

  SimilarBooks({
    this.allBooks,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Container(
      height: 95,
      alignment: Alignment.center,
      child: ListView.builder(
        itemCount: allBooks.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          Book book = allBooks[index];
          return bookItem(book);
        },
      ),
    );
  }

  Widget bookItem(Book book) {
    return DefaultButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BookPage(
              book: book,
              allBooks: allBooks,
            ),
          ),
        );
      },
      child: Container(
        width: 75,
        height: 90,
        child: MyNetworkImage(
          borderRadius: 10,
          imageUrl: book.imageUrl,
        ),
      ),
    );
  }
}
