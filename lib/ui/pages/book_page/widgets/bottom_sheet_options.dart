import 'package:booklibrary/blocs/book/book_bloc.dart';
import 'package:booklibrary/constants/values/route_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/default_bottom_sheet.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/ui/widgets/text/text17_semibold.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:share/share.dart';

class BottomSheetOptions extends StatelessWidget {
  final Book book;
  final BookBloc bloc;
  BuildContext context;

  BottomSheetOptions({
    this.book,
    this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return DefaultBottomSheet(
      child: Column(
        children: <Widget>[
          dataRow('edit', () {
            Navigator.pop(context);

            Navigator.pushNamed(context, MyRoutes.bookCreate, arguments: book);
          }),
          dataRow('delete', () {
            showAlert();
          }),
          dataRow(
            'share',
            () {
              Share.share(
                'Название книги: ${book.title} \n\nОписание: ${book.description} \n\nЦена: ${book.price}',
              );
            },
          ),
        ],
      ),
    );
  }

  Widget dataRow(String text, Function onPressed) {
    return DefaultButton(
      onPressed: () {
        onPressed();
      },
      height: 52,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Text17s(
          AppTranslations.of(context).text(text),
          textAlign: TextAlign.start,
        ),
      ),
    );
  }

  showAlert() {
    Alert(
      context: context,
      type: AlertType.none,
      title: AppTranslations.of(context).text('deleteDesc'),
      buttons: [
        DialogButton(
          child: Text17(
            AppTranslations.of(context).text('no'),
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          width: 120,
        ),
        DialogButton(
          child: Text17(
            AppTranslations.of(context).text('yes'),
            color: Colors.white,
          ),
          onPressed: () {
            bloc.add(DeleteBook(book));
          },
          width: 120,
        )
      ],
    ).show();
  }
}
