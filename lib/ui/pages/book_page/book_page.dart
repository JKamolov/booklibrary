import 'package:booklibrary/blocs/book/book_bloc.dart';
import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/defaults/default_app_bar.dart';
import 'package:booklibrary/ui/widgets/states/ontop_loading_state.dart';
import 'package:booklibrary/ui/widgets/text/default_text.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/book_desc.dart';
import 'widgets/book_info.dart';
import 'widgets/bottom_sheet_options.dart';
import 'widgets/similar_books.dart';

class BookPage extends StatefulWidget {
  final Book book;
  final List<Book> allBooks;

  BookPage({
    this.book,
    this.allBooks,
  });

  @override
  _BookPageState createState() => _BookPageState();
}

class _BookPageState extends State<BookPage> {
  bool loading = false;
  bool expandableOpen = false;
  ExpandableController expandableController =
      ExpandableController(initialExpanded: false);

  @override
  void initState() {
    super.initState();
  }

  BookBloc bloc;
  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BookBloc(),
      child: BlocBuilder<BookBloc, BookState>(
        builder: (context, state) {
          bloc = BlocProvider.of<BookBloc>(context);
          bloc.context = context;
          if (state is ExpandableStateUpdated) {
            expandableOpen = state.open;
            expandableController.expanded = state.open;
          }
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: MyAppBar(
              title: widget.book.title,
              actions: <Widget>[
                IconButton(
                  onPressed: () {
                    showBottom();
                  },
                  icon: Icon(
                    Icons.more_vert,
                    color: MyColors.secondaryText,
                  ),
                )
              ],
            ),
            body: Stack(
              children: <Widget>[
                getBody(),
                loading ? MyOnTopLoadingState() : Container()
              ],
            ),
          );
        },
      ),
    );
  }

  Widget getBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
        vertical: 24,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BookInfo(book: widget.book),
          SizedBox(
            height: 16,
          ),
          BookDesc(
            book: widget.book,
            expandableOpen: expandableOpen,
            expandableController: expandableController,
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: MyText(
              AppTranslations.of(context).text('similarBook'),
              fontFamily: MyFonts.bold,
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          SimilarBooks(
            allBooks: widget.allBooks,
          )
        ],
      ),
    );
  }

  showBottom() {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return BottomSheetOptions(
          book: widget.book,
          bloc: bloc,
        );
      },
    );
  }
}
