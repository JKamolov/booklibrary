import 'package:booklibrary/blocs/settings_page/settings_bloc.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/default_app_bar.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/ui/widgets/text/text17_semibold.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingaPage extends StatefulWidget {
  @override
  _SettingaPageState createState() => _SettingaPageState();
}

class _SettingaPageState extends State<SettingaPage> {
  SettingsBloc bloc;
  String currentLang;
  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => SettingsBloc(),
        child: BlocBuilder<SettingsBloc, SettingsState>(
          builder: (context, state) {
            bloc = BlocProvider.of<SettingsBloc>(context);
            bloc.context = context;
            if (state is SettingsInitial) {
              currentLang = AppTranslations.of(context).currentLanguage;
            } else if (state is LanguageUpdated) {
              currentLang = state.language;
            }
            return Scaffold(
              backgroundColor: Colors.white,
              appBar:
                  MyAppBar(title: AppTranslations.of(context).text('settings')),
              body: getBody(),
            );
          },
        ));
  }

  Widget getBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
        vertical: 24,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text17s(AppTranslations.of(context).text('language'))),
          SizedBox(
            height: 16,
          ),
          langRow('Русский', 'ru'),
          langRow('English', 'en'),
          langRow('O\'zbek', 'uz')
        ],
      ),
    );
  }

  Widget langRow(String text, String code) {
    bool selected = code == currentLang;
    return DefaultButton(
      radius: 0,
      height: 48,
      onPressed: () {
        bloc.add(UpdateLanguage(code));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text17(text),
            selected
                ? Icon(
                    Icons.done,
                    color: MyColors.accent,
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
