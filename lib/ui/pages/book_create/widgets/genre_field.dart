import 'package:booklibrary/constants/ui_values/ui_values.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/textfield/default_textfield_container.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:booklibrary/utils/other_utils.dart';
import 'package:flutter/material.dart';

import 'genre_list.dart';

class GenreField extends StatelessWidget {
  final TextEditingController controller;
  final Book book;
  final bool autoValidate;
  BuildContext context;

  GenreField({
    this.book,
    this.autoValidate,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return InkWell(
      onTap: () {
        showBottom();
      },
      child: DefaultTextFieldContainer(
        isRequired: true,
        controller: controller,
        autoValidate: autoValidate,
        showRequiredSign: true,
        enabled: false,
        labelText: AppTranslations.of(context).text('genre'),
      ),
    );
  }

  showBottom() {
    return showModalBottomSheet(
        context: context,
        // isScrollControlled: true,
        builder: (context) {
          return GenreListBottom(
            selectedIndex: book.genre,
            genres: gendres(context),
          );
        }).then(
      (value) {
        if (value != null) {
          book.genre = value;
          controller.text = Utils.getGenreText(context, book.genre);
        }
      },
    );
  }
}
