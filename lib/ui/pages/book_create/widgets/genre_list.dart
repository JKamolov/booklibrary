import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/default_bottom_sheet.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/other_utils.dart';
import 'package:flutter/material.dart';

class GenreListBottom extends StatelessWidget {
  final List<String> genres;
  final int selectedIndex;
  BuildContext context;

  GenreListBottom({this.genres, this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return DefaultBottomSheet(
      child: Column(
        children: genres
            .map(
              (e) => item(
                genres.indexOf(e),
              ),
            )
            .toList(),
      ),
    );
  }

  Widget item(int index) {
    bool selected = selectedIndex == index;
    return DefaultButton(
      color: Colors.transparent,
      radius: 0,
      height: 32,
      onPressed: () {
        Navigator.pop(
          context,
          index,
        );
      },
      child: Container(
        height: 32,
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 16),
        width: MediaQuery.of(context).size.width,
        child: Text17(
          Utils.getGenreText(context, index),
          color: selected ? MyColors.accent : Colors.black,
        ),
      ),
    );
  }
}
