import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/pages/book_create/widgets/genre_field.dart';
import 'package:booklibrary/ui/widgets/textfield/default_textfield_container.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'description_field.dart';
import 'image_url_field.dart';

class BookForm extends StatelessWidget {
  final Book book;
  final GlobalKey<FormState> formKey;
  final bool autoValidate;
  final TextEditingController descriptionController,
      urlController,
      genreController;

  BookForm({
    this.book,
    this.formKey,
    this.descriptionController,
    this.genreController,
    this.autoValidate,
    this.urlController,
  });

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: <Widget>[
          DefaultTextFieldContainer(
            isRequired: true,
            initialValue: book.title,
            showRequiredSign: true,
            autoValidate: autoValidate,
            labelText: AppTranslations.of(context).text('name'),
            onSaved: (text) {
              book.title = text;
            },
          ),
          SizedBox(
            height: 16,
          ),
          DefaultTextFieldContainer(
            isRequired: true,
            showRequiredSign: true,
            autoValidate: autoValidate,
            initialValue: book.author,
            labelText: AppTranslations.of(context).text('author'),
            onSaved: (text) {
              book.author = text;
            },
          ),
          SizedBox(
            height: 16,
          ),
          DescriptionField(
            controller: descriptionController,
            autoValidate: autoValidate,
          ),
          SizedBox(
            height: 16,
          ),
          DefaultTextFieldContainer(
            isRequired: true,
            showRequiredSign: true,
            autoValidate: autoValidate,
            initialValue:
                book.publishedYear == null ? '' : book.publishedYear.toString(),
            inputType: TextInputType.number,
            inputFormatters: [
              WhitelistingTextInputFormatter(RegExp("[1234567890]")),
            ],
            labelText: AppTranslations.of(context).text('publishedYear'),
            onSaved: (text) {
              book.publishedYear = int.parse(text);
            },
          ),
          SizedBox(
            height: 16,
          ),
          DefaultTextFieldContainer(
            isRequired: true,
            showRequiredSign: true,
            autoValidate: autoValidate,
            onSaved: (text) {
              book.price = text;
            },
            initialValue: book.price == null ? '' : book.price.toString(),
            inputType: TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              WhitelistingTextInputFormatter(RegExp("[1234567890.]")),
            ],
            labelText: AppTranslations.of(context).text('price'),
          ),
          SizedBox(
            height: 16,
          ),
          GenreField(
            book: book,
            autoValidate: autoValidate,
            controller: genreController,
          ),
          SizedBox(
            height: 16,
          ),
          ImageUrlField(
            controller: urlController,
            autoValidate: autoValidate,
          )
        ],
      ),
    );
  }
}
