import 'package:booklibrary/constants/assets/icon_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/ui/widgets/textfield/default_textfield_container.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DescriptionField extends StatelessWidget {
  final TextEditingController controller;
  final bool autoValidate;

  DescriptionField({
    this.controller,
    this.autoValidate,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: DefaultTextFieldContainer(
            height: 200,
            maxLines: 30,
            autoValidate: autoValidate,
            contentPadding: EdgeInsets.only(right: 32),
            isRequired: true,
            showRequiredSign: true,
            textInputAction: TextInputAction.newline,
            controller: controller,
            labelText: AppTranslations.of(context).text('description'),
          ),
        ),
        Positioned(
          right: 16,
          top: 16,
          child: IconButton(
            onPressed: () async {
              setText();
            },
            icon: SvgPicture.asset(
              MyIcons.paste,
              color: MyColors.secondaryText,
              width: 24,
              height: 24,
            ),
          ),
        )
      ],
    );
  }

  setText() async {
    ClipboardData clipboardData = await Clipboard.getData('text/plain');
    if (clipboardData != null) {
      if (clipboardData.text != null && clipboardData.text.isNotEmpty) {
        String currentValue = controller.text;
        if (currentValue.isNotEmpty) {
          controller.text = currentValue + '\n' + clipboardData.text;
        } else {
          controller.text = clipboardData.text;
        }
      }
    }
  }
}
