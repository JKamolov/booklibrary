import 'package:booklibrary/blocs/book_create/bookcreate_bloc.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/default_app_bar.dart';
import 'package:booklibrary/ui/widgets/states/ontop_loading_state.dart';
import 'package:booklibrary/ui/widgets/text/text17_semibold.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:booklibrary/utils/other_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/form.dart';

class BookCreate extends StatefulWidget {
  final Book book;
  BookCreate({this.book});
  @override
  _BookCreateState createState() => _BookCreateState();
}

class _BookCreateState extends State<BookCreate> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController imageUrlController = TextEditingController();
  TextEditingController genreController = TextEditingController();

  bool loading = false;
  BookcreateBloc bloc;
  Book book;
  bool autoValidate = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BookcreateBloc(),
      child: BlocBuilder<BookcreateBloc, BookcreateState>(
        builder: (context, state) {
          bloc = BlocProvider.of<BookcreateBloc>(context);
          bloc.context = context;
          if (state is BookcreateInitial) {
            if (widget.book != null) {
              book = widget.book;
              descriptionController.text = book.description;
              genreController.text = book.genre != null
                  ? Utils.getGenreText(context, book.genre)
                  : '';
              imageUrlController.text = book.imageUrl;
            } else {
              book = Book();
            }
          } else if (state is LoadingState) {
            loading = state.loading;
          } else if (state is AutoValidateUpdated) {
            autoValidate = state.autoValidate;
          }
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: MyAppBar(
                title: AppTranslations.of(context)
                    .text(widget.book == null ? 'newBook' : 'editBook')),
            body: Stack(children: <Widget>[
              getBody(),
              loading ? MyOnTopLoadingState() : Container()
            ]),
          );
        },
      ),
    );
  }

  Widget getBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
      child: Column(
        children: <Widget>[
          BookForm(
            formKey: formKey,
            book: book,
            descriptionController: descriptionController,
            urlController: imageUrlController,
            autoValidate: autoValidate,
            genreController: genreController,
          ),
          SizedBox(
            height: 16,
          ),
          button()
        ],
      ),
    );
  }

  Widget button() {
    return DefaultButton(
      height: 52,
      color: MyColors.accent,
      onPressed: () {
        if (formKey.currentState.validate()) {
          book.description = descriptionController.text;
          book.imageUrl = imageUrlController.text;
          formKey.currentState.save();
          if (widget.book == null) {
            bloc.add(CreateBook(book));
          } else {
            bloc.add(UpdateBook(book));
          }
        } else {
          bloc.add(UpdateAutoValidate(true));
        }
      },
      child: Center(
        child: Text17s(
            AppTranslations.of(context).text(
              widget.book == null ? 'create' : 'update',
            ),
            color: Colors.white),
      ),
    );
  }
}
