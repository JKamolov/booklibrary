import 'package:booklibrary/constants/assets/font_constants.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/pages/book_page/book_page.dart';
import 'package:booklibrary/ui/widgets/button/default_button.dart';
import 'package:booklibrary/ui/widgets/defaults/network_image.dart';
import 'package:booklibrary/ui/widgets/text/default_text.dart';
import 'package:booklibrary/ui/widgets/text/text15.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/other_utils.dart';
import 'package:flutter/material.dart';

class BookItem extends StatelessWidget {
  final Book book;
  final List<Book> allBooks;

  BookItem({
    this.book,
    this.allBooks,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultButton(
      color: Colors.white,
      radius: 0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BookPage(
              book: book,
              allBooks: allBooks,
            ),
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: MyColors.divider,
            ),
          ),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              picture(),
              SizedBox(
                width: 16,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    MyText(
                      book.title,
                      fontFamily: MyFonts.semibold,
                    ),
                    // SizedBox(
                    //   height: 3,
                    // ),
                    Text17(
                      book.author,
                      // color: MyColors.secondaryText,
                    ),
                    Text15(
                      Utils.getGenreText(context, book.genre),
                      color: MyColors.secondaryText,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget picture() {
    return Container(
      width: 82,
      height: 115,
      child: MyNetworkImage(
        imageUrl: book.imageUrl,
        borderRadius: 12,
        fit: BoxFit.fill,
      ),
    );
  }
}
