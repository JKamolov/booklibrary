import 'package:booklibrary/blocs/home/home_bloc.dart';
import 'package:booklibrary/constants/values/color_constants.dart';
import 'package:booklibrary/constants/values/route_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/ui/widgets/defaults/default_app_bar.dart';
import 'package:booklibrary/ui/widgets/states/loading_state.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'widgets/book_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController tabController;
  HomeBloc bloc;
  List<Book> books = [];
  bool loading = true;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      bloc.add(LoadInitialData());
    });
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(),
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          bloc = BlocProvider.of<HomeBloc>(context);
          if (state is BooksLoaded) {
            loading = false;
            books = state.books;
          }
          return Scaffold(
            appBar: MyAppBar(
              title: AppTranslations.of(context).text('books'),
              leading: Container(),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.settings, color: MyColors.additionalText),
                  onPressed: () {
                    // ShPrefHelper.setFirtLog(true);
                    Navigator.pushNamed(context, MyRoutes.settingsPage);
                  },
                )
              ],
            ),
            body: loading ? MyLoadingState() : getBody(),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, MyRoutes.bookCreate);
              },
              child: Icon(Icons.add, color: Colors.white),
            ),
          );
        },
      ),
    );
  }

  Widget getBody() {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: books.length,
      itemBuilder: (context, index) {
        Book book = books[index];
        return BookItem(
          book: book,
          allBooks: books,
        );
      },
    );
  }
}
