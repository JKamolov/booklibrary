part of 'bookcreate_bloc.dart';

abstract class BookcreateEvent extends Equatable {
  const BookcreateEvent();
}

class CreateBook extends BookcreateEvent {
  final Book book;

  CreateBook(this.book);

  @override
  List<Object> get props => [book];
}

class UpdateBook extends BookcreateEvent {
  final Book book;

  UpdateBook(this.book);

  @override
  List<Object> get props => [book];
}

class UpdateAutoValidate extends BookcreateEvent {
  final bool autoValidate;

  UpdateAutoValidate(this.autoValidate);

  @override
  List<Object> get props => [autoValidate];
}
