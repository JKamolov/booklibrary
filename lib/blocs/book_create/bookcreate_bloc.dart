import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:booklibrary/constants/values/route_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/services/sql/sql_operations.dart';
import 'package:booklibrary/ui/widgets/text/text17.dart';
import 'package:booklibrary/utils/localization/app_translations.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

part 'bookcreate_event.dart';
part 'bookcreate_state.dart';

class BookcreateBloc extends Bloc<BookcreateEvent, BookcreateState> {
  @override
  BookcreateState get initialState => BookcreateInitial();
  BuildContext context;
  @override
  Stream<BookcreateState> mapEventToState(
    BookcreateEvent event,
  ) async* {
    if (event is CreateBook) {
      yield* createBook(event.book);
    } else if (event is UpdateBook) {
      yield* update(event.book);
    } else if (event is UpdateAutoValidate) {
      yield AutoValidateUpdated(event.autoValidate);
    }
  }

  Stream<BookcreateState> createBook(Book book) async* {
    yield LoadingState(true);
    await SqlOp.createBook(book);
    yield LoadingState(false);
    showAlert();
  }

  Stream<BookcreateState> update(Book book) async* {
    yield LoadingState(true);
    await SqlOp.updateBook(book);
    yield LoadingState(false);
    showAlert();
  }

  showAlert() {
    Alert(
      context: context,
      type: AlertType.success,
      title: AppTranslations.of(context).text('saved'),
      // desc: AppTranslations.of(context).text('deleteDesc'),
      buttons: [
        DialogButton(
          child: Text17(
            AppTranslations.of(context).text('ok'),
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pushNamedAndRemoveUntil(
                context, MyRoutes.homePage, (route) => false);
          },
          width: 120,
        ),
      ],
    ).show();
  }
}
