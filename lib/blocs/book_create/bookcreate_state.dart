part of 'bookcreate_bloc.dart';

abstract class BookcreateState extends Equatable {
  const BookcreateState();
}

class BookcreateInitial extends BookcreateState {
  @override
  List<Object> get props => [];
}

class LoadingState extends BookcreateState {
  final bool loading;

  LoadingState(this.loading);

  @override
  List<Object> get props => [loading];
}

class AutoValidateUpdated extends BookcreateState {
  final bool autoValidate;

  AutoValidateUpdated(this.autoValidate);

  @override
  List<Object> get props => [autoValidate];
}
