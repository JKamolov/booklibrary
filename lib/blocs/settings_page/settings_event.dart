part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();
}

class UpdateLanguage extends SettingsEvent {
  final String lang;

  UpdateLanguage(this.lang);

  @override
  List<Object> get props => [lang];
}
