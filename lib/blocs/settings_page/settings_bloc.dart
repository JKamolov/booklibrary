import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:booklibrary/utils/localization/application.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  @override
  SettingsState get initialState => SettingsInitial();
  BuildContext context;
  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    if (event is UpdateLanguage) {
      application.onLocaleChanged(Locale(event.lang));

      yield LanguageUpdated(event.lang);
    }
  }
}
