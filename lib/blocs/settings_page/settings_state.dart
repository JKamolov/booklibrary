part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();
}

class SettingsInitial extends SettingsState {
  @override
  List<Object> get props => [];
}

class LanguageUpdated extends SettingsState {
  final String language;

  LanguageUpdated(this.language);

  @override
  List<Object> get props => [language];
}
