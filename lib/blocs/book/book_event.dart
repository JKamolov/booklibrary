part of 'book_bloc.dart';

abstract class BookEvent extends Equatable {
  const BookEvent();
}

class DeleteBook extends BookEvent {
  final Book book;

  DeleteBook(this.book);
  @override
  List<Object> get props => [book];
}

class UpdateExpandableState extends BookEvent {
  final bool open;

  UpdateExpandableState(this.open);

  @override
  List<Object> get props => [open];
}
