import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:booklibrary/constants/values/route_constants.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/services/sql/sql_operations.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'book_event.dart';
part 'book_state.dart';

class BookBloc extends Bloc<BookEvent, BookState> {
  @override
  BookState get initialState => BookInitial();
  BuildContext context;
  @override
  Stream<BookState> mapEventToState(
    BookEvent event,
  ) async* {
    if (event is DeleteBook) {
      yield LoadingState(true);
      await SqlOp.deleteBook(event.book.id);
      yield LoadingState(false);
      Navigator.pushNamedAndRemoveUntil(
          context, MyRoutes.homePage, (route) => false);
    } else if (event is UpdateExpandableState) {
      yield ExpandableStateUpdated(event.open);
    }
  }
}
