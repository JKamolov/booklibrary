part of 'book_bloc.dart';

abstract class BookState extends Equatable {
  const BookState();
}

class BookInitial extends BookState {
  @override
  List<Object> get props => [];
}

class LoadingState extends BookState {
  final bool loading;

  LoadingState(this.loading);

  @override
  List<Object> get props => [loading];
}

class ExpandableStateUpdated extends BookState {
  final bool open;

  ExpandableStateUpdated(this.open);

  @override
  List<Object> get props => [open];
}
