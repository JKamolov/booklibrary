part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class BooksLoaded extends HomeState {
  final List<Book> books;

  BooksLoaded(this.books);

  @override
  List<Object> get props => [books];
}
