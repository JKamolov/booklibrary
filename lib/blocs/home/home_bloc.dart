import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:booklibrary/constants/ui_values/ui_values.dart';
import 'package:booklibrary/models/api/book/book.dart';
import 'package:booklibrary/services/sql/sql_operations.dart';
import 'package:booklibrary/utils/shared_pref/shared_pref_helper.dart';
import 'package:equatable/equatable.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is LoadInitialData) {
      yield* loadBooks();
    }
  }

  Stream<HomeState> loadBooks() async* {
    bool firstLog = ShPrefHelper.firstLog();
    if (firstLog) {
      await SqlOp.fillWithData(recommendedBooks);
      ShPrefHelper.setFirtLog(false);
    }
    List<Book> books = await SqlOp.getBooks();
    yield BooksLoaded(books);
  }
}
